#!/usr/bin/env bash

set -e
./mvnw clean package
docker build -t "back:latest" .