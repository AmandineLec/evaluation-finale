package zenika.evaluation.back;



import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print; 
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT )
@AutoConfigureMockMvc
public class ProjectControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Test
    public void getProject() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders
            .get("/api/project")
            .accept(MediaType.APPLICATION_JSON)).andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    public void getProjectByHisId() throws Exception{
        Integer id = 1; 
        mockMvc.perform(MockMvcRequestBuilders
        .get("/api/project/" + id)
        .accept(MediaType.APPLICATION_JSON)).andDo(print())
        .andExpect(status().isOk());
    }

    @Test
    public void getTicketByHisId() throws Exception{
        Integer id = 1; 
        mockMvc.perform(MockMvcRequestBuilders
        .get("/api/project/ticket/" + id)
        .accept(MediaType.APPLICATION_JSON)).andDo(print())
        .andExpect(status().isOk());
    }
}
