package zenika.evaluation.back.exception;

public class ProjectException extends Exception{
    public ProjectException(String message){
        super(message);
    }
}
