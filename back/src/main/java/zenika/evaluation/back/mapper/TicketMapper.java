package zenika.evaluation.back.mapper;

import org.springframework.stereotype.Component;

import zenika.evaluation.back.dto.TicketDto;
import zenika.evaluation.back.model.Ticket;

@Component
public class TicketMapper {
    public Ticket ticketDtoToTicket(TicketDto ticketDto){
        Ticket ticket = new Ticket();
        ticket.setTitle(ticketDto.getTitle());
        ticket.setContent(ticketDto.getContent());
        ticket.setEtat(ticketDto.getEtat());
        return ticket; 
    }

    public TicketDto ticketToTicketDto(Ticket ticket){
        TicketDto ticketDto = new TicketDto();
        ticketDto.setTitle(ticket.getTitle());
        ticketDto.setDate(ticket.getDate());
        ticketDto.setContent(ticket.getContent());
        ticketDto.setId(ticket.getId());
        ticketDto.setEtat(ticket.getEtat());
        return ticketDto; 
    }
}
