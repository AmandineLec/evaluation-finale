package zenika.evaluation.back.mapper;

import org.springframework.stereotype.Component;

import zenika.evaluation.back.dto.ProjectDto;
import zenika.evaluation.back.dto.TicketDto;
import zenika.evaluation.back.model.Project;
import zenika.evaluation.back.model.Ticket;

@Component
public class ProjectMapper {
    
    TicketMapper tMapper; 
    UserMapper uMapper; 

    public ProjectMapper(TicketMapper tMapper, UserMapper uMapper){
        this.tMapper = tMapper; 
        this.uMapper = uMapper; 
    }

    public Project projectDtoToProject(ProjectDto projectDto){
        Project project = new Project();
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setDate(projectDto.getDate());
        for(TicketDto ticketDto : projectDto.getTickets()){
            project.getTickets().add(tMapper.ticketDtoToTicket(ticketDto));
            ticketDto.setProject(projectDto);
        }
        if(projectDto.getUser() != null){
            project.setUser(uMapper.userDtoToUser(projectDto.getUser()));
        }
        return project; 
    }

    public ProjectDto projectToProjectDto(Project project){
        ProjectDto projectDto = new ProjectDto();
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        projectDto.setDate(project.getDate());
        for(Ticket ticket : project.getTickets()){
            projectDto.getTickets().add(tMapper.ticketToTicketDto(ticket));
            ticket.setProject(project);
        }
        projectDto.setId(project.getId());
        if(project.getUser() != null){
            projectDto.setUser(uMapper.userToUserDto(project.getUser()));
        }
        return projectDto; 
    }
}
