package zenika.evaluation.back.mapper;

import org.springframework.stereotype.Component;

import zenika.evaluation.back.dto.UserDto;
import zenika.evaluation.back.model.User;

@Component
public class UserMapper {
    

    public User userDtoToUser(UserDto userDto){
        User user = new User(); 
        user.setUsername(userDto.getUsername());
        user.setMail(userDto.getMail());
        user.setPassword(userDto.getPassword());
        user.setFirstName(userDto.getFirstName());
        user.setLastname(userDto.getLastName());
        return user; 
    }

    public UserDto userToUserDto(User user){
        UserDto userDto = new UserDto();
        userDto.setUsername(user.getUsername());
        userDto.setMail(user.getMail());
        userDto.setPassword(user.getPassword());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastname());
        return userDto; 
    }
}
