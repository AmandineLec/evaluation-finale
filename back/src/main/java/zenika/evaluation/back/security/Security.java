package zenika.evaluation.back.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;


@Configuration
@EnableWebSecurity
public class Security {
    //Permet d'encrypter les mot de passe en Bcrypt
    @Bean 
    public PasswordEncoder getEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    //Permet de spécifier les routes nécéssitant une authentification ou non en focntion des requêtes http
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
        http.
            authorizeHttpRequests()
            .requestMatchers("/api/project/**").permitAll()
            .requestMatchers("/swagger-ui.html","/swagger-ui/**", "/v3/api-docs/**").permitAll()
            .requestMatchers("/api/inscription").permitAll()
            .anyRequest()
            .authenticated()
            .and()
            //Permet d'enlever la pop-up de sping security
            .httpBasic(httpSecurityHttpBasicConfigurer -> httpSecurityHttpBasicConfigurer.authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED)));
        //La session en stateless permet de ne pas stocker de cookie
        http.sessionManagement(management -> management
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS));   
        //vient désactiv le CSRF 
        http.csrf().disable();
        return http.build();
    } 


}
