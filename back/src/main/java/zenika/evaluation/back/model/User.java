package zenika.evaluation.back.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name="\"user\"")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id; 

    @Column(name="last_name")
    private String lastname; 

    @Column(name="first_name")
    private String firstName; 

    @Column(name="username")
    private String username; 

    @Column(name="password")
    private String password; 

    @Column(name="mail")
    private String mail;

    @OneToMany
    private List<Project> projects = new ArrayList<>(); 
}
