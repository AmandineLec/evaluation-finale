package zenika.evaluation.back.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name="project")
public class Project {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id; 

    @Column(name="name")
    private String name; 

    @Column(name="description", length = 500)
    private String description;

    @Column(name="date")
    private LocalDate date = LocalDate.now(); 

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user; 

    @OneToMany
    private List<Ticket> tickets = new ArrayList<>(); 


}
