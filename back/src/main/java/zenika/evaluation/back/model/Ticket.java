package zenika.evaluation.back.model;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import zenika.evaluation.back.enumeration.Etat;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name="ticket")
public class Ticket {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id; 

    @Column(name="title", length= 50)
    private String title; 

    @Column(name="content", length = 1000)
    private String content;

    @Column(name="date")
    private LocalDate date;

    @Column(name="etat")
    private Etat etat; 

    @ManyToOne
    @JoinColumn(name="project_id")
    private Project project; 
    
}
