package zenika.evaluation.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import zenika.evaluation.back.model.Project;

public interface ProjectRepository extends JpaRepository<Project, Integer>{
    
}
