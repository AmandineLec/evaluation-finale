package zenika.evaluation.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import zenika.evaluation.back.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{
    User findByUsername(String username);
}
