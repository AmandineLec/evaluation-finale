package zenika.evaluation.back.service;


import java.time.LocalDate;

import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import zenika.evaluation.back.dao.TicketRepository;
import zenika.evaluation.back.enumeration.Etat;
import zenika.evaluation.back.exception.TicketException;
import zenika.evaluation.back.model.Project;
import zenika.evaluation.back.model.Ticket;

@Service
public class TicketService {
    TicketRepository tRepository; 

    public TicketService(TicketRepository tRepository){
        this.tRepository = tRepository; 
    }

    //Allow to add a new ticket in the database
    public Ticket newTicket(Project project, Ticket ticket, String etat){
        ticket.setProject(project);
        //we set the date of creation thanks to local date and we get the date of the day
        ticket.setDate(LocalDate.now());
        project.getTickets().add(ticket);
        //We sset the status of the ticket thanks a method that convert a string into an enumeration (see behind)
        ticket.setEtat(updateEtat(etat));
        return tRepository.save(ticket); 
    }

    //Allow to partially modify a ticket thanks to his id
    @Transactional
    public Ticket updateTicket(Integer ticketId, Ticket  updateTicket, String etat){
        //We get the ticket to update in the database
        Ticket ticket = tRepository.findById(ticketId).orElse(null); //TO DO : gestion des erreurs
        //We set the different attribute with the ticket in the parameters
        ticket.setTitle(updateTicket.getTitle());
        ticket.setContent(updateTicket.getContent());
        ticket.setProject(updateTicket.getProject());
        ticket.setEtat(updateEtat(etat));
        return tRepository.save(ticket);
    }

    //get the status in the enumeration thanks to a string
    public Etat updateEtat(String etat){        
        Etat newEtat = Etat.valueOf(etat);
        return newEtat; 
    }

    //Allow to delete a ticket thanks to his id
    @Transactional
    public void deleteTicket(Integer ticketId) throws TicketException{
        Ticket ticket = tRepository.findById(ticketId).orElseThrow(()-> new TicketException("ticket not found")); 
        ticket.getProject().getTickets().remove(ticket);
        tRepository.delete(ticket);
    }   
}
