package zenika.evaluation.back.service;

import org.springframework.stereotype.Service;

import zenika.evaluation.back.dao.UserRepository;
import zenika.evaluation.back.model.User;

@Service
public class UserService {
    UserRepository uRepository; 
    public UserService(UserRepository uRepository){
        this.uRepository = uRepository;
    }
    //Allow to add a new user in the database
    public User newUser(User user){
        return uRepository.save(user);
    }
}
