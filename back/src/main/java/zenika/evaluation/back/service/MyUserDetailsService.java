package zenika.evaluation.back.service;

import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import zenika.evaluation.back.dao.UserRepository;
import zenika.evaluation.back.model.MyUserPrincipal;
import zenika.evaluation.back.model.User;

@Service
//override of the UserDetalsService to manage and save  the user in the database
public class MyUserDetailsService implements UserDetailsService {
    
    private UserRepository uRepository; 

    public MyUserDetailsService(UserRepository uRepository){
        this.uRepository = uRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = uRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new MyUserPrincipal(user);
    }
}
