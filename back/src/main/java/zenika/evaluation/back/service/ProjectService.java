package zenika.evaluation.back.service;

import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import zenika.evaluation.back.dao.ProjectRepository;
import zenika.evaluation.back.exception.ProjectException;
import zenika.evaluation.back.exception.TicketException;
import zenika.evaluation.back.model.Project;
import zenika.evaluation.back.model.Ticket;
import zenika.evaluation.back.model.User;

@Service
public class ProjectService {

    ProjectRepository pRepository;
    TicketService tService; 

    public ProjectService(ProjectRepository pRepository, TicketService tService){
        this.pRepository = pRepository; 
        this.tService = tService; 
    }
    
    //allow to add a new project in the database thanks to his id and the user that wants to add the project. 
    public Project newProject(User user, Project project){
        //the user is setted to the project
        project.setUser(user);
        //we add the project to the project's list of the user
        user.getProjects().add(project);
        //we save it in the database
        return pRepository.save(project);
    }

    @Transactional
    //allow to partially modify a project thanks to his id
    public Project updateProject(Integer projectId, Project updateProject) throws ProjectException{
        //we get the project in the databse thanks to his id and we modify the data thanks to the project in the paramaters
        Project project = pRepository.findById(projectId).orElseThrow(()-> new ProjectException("project not found"));
        project.setName(updateProject.getName());
        project.setDescription(updateProject.getDescription());
        return pRepository.save(project);
    }

    //allow to delete a project and all the tickets of the project thanks to his id
    @Transactional
    public void deleteProject(Integer projectId) throws ProjectException, TicketException{
        //we get the project that we want to delete in the database
        Project project = pRepository.findById(projectId).orElseThrow(()-> new ProjectException("project not found")); 
        //we get all the tickets that are in the list of the project and we use a method in the ticket service to delete the ticket thanks to their id
        for(Ticket ticket : project.getTickets()){
            tService.deleteTicket(ticket.getId());
        }
        //We remove the ticket of the list
        project.getUser().getProjects().remove(project);
        //We delete the project
        pRepository.delete(project);
    }   

}
