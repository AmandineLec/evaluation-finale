package zenika.evaluation.back.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import zenika.evaluation.back.dto.UserDto;
import zenika.evaluation.back.mapper.UserMapper;
import zenika.evaluation.back.model.User;
import zenika.evaluation.back.service.UserService;

@RestController
@RequestMapping("api/inscription")
//Controller to register an user in the database
public class InscriptionController {

    UserService uService; 
    UserMapper uMapper; 
    PasswordEncoder passwordEncoder; 

    public InscriptionController(UserService uService, UserMapper uMapper, PasswordEncoder passwordEncoder){
        this.uService = uService;
        this.uMapper = uMapper; 
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping
    public ResponseEntity<User> newUser(@RequestBody UserDto userDto){
        //the password is encrypted before saving the user in the database. Otherwise, the password is not recognized and the user can not log in
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        return ResponseEntity.ok(this.uService.newUser(uMapper.userDtoToUser(userDto)));
    }
}
