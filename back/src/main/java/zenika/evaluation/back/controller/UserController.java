package zenika.evaluation.back.controller;


import java.security.Principal;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import zenika.evaluation.back.dao.ProjectRepository;
import zenika.evaluation.back.dao.UserRepository;
import zenika.evaluation.back.dto.ProjectDto;
import zenika.evaluation.back.dto.TicketDto;
import zenika.evaluation.back.exception.ProjectException;
import zenika.evaluation.back.exception.TicketException;
import zenika.evaluation.back.mapper.ProjectMapper;
import zenika.evaluation.back.mapper.TicketMapper;
import zenika.evaluation.back.mapper.UserMapper;
import zenika.evaluation.back.model.MyUserPrincipal;
import zenika.evaluation.back.model.Project;
import zenika.evaluation.back.model.Ticket;
import zenika.evaluation.back.model.User;
import zenika.evaluation.back.service.ProjectService;
import zenika.evaluation.back.service.TicketService;

@RestController
@RequestMapping("/api/user")
@SecurityRequirement(name = "basicAuth")
//Controller for all the routes that needs an authentication
public class UserController {

    ProjectService pService;
    UserRepository uRepository; 
    ProjectRepository projectRepository; 
    TicketService tService; 
    ProjectMapper pMapper; 
    TicketMapper tMapper; 
    UserMapper uMapper; 

    public UserController(ProjectService pService, UserRepository uRepository, ProjectRepository projectRepository, TicketService tService, ProjectMapper pMapper, TicketMapper tMapper, UserMapper uMapper){
        this.pService = pService; 
        this.uRepository = uRepository; 
        this.projectRepository = projectRepository; 
        this.tService = tService; 
        this.pMapper = pMapper;
        this.tMapper = tMapper; 
        this.uMapper = uMapper; 
    }
    //allow to add a new project in the database
    @PostMapping("/project")
    public ResponseEntity<ProjectDto> addNewproject(Principal principal, @RequestBody ProjectDto projectDto){
        User user = uRepository.findByUsername(principal.getName()); 
        Project project = pMapper.projectDtoToProject(projectDto);
        return ResponseEntity.ok(pMapper.projectToProjectDto(pService.newProject(user,project)));
    }

    //allow to add a new ticket in the database
    @PostMapping("/ticket/{id}")
    public ResponseEntity<TicketDto> addNewTicket(@PathVariable Integer id, @RequestBody TicketDto ticketdto, @RequestParam String etat) throws ProjectException{
        Project project = projectRepository.findById(id).orElseThrow(()-> new ProjectException("project not found"));
        Ticket ticket = tMapper.ticketDtoToTicket(ticketdto);        
        return ResponseEntity.ok(tMapper.ticketToTicketDto(tService.newTicket(project, ticket, etat)));
    }

    //allow to modify partially a project thanks to his id
    @PatchMapping("/project/{id}")
    public ResponseEntity<ProjectDto> updateProject(@PathVariable Integer id,  @RequestBody ProjectDto projectDto) throws ProjectException{
        Project project = pService.updateProject(id, pMapper.projectDtoToProject(projectDto));
        return ResponseEntity.ok(pMapper.projectToProjectDto(project));
    }

    //allow to modify partially a ticket thanks to his id
    @PatchMapping("/ticket/{idTicket}")
    public ResponseEntity<TicketDto> updateTicket(@PathVariable Integer idTicket, @RequestBody TicketDto ticketDto, @RequestParam String etat){
        Ticket ticket = tService.updateTicket(idTicket, tMapper.ticketDtoToTicket(ticketDto), etat);
        return ResponseEntity.ok(tMapper.ticketToTicketDto(ticket));
    }

    //allow to delete a project thanks to his id
    @DeleteMapping("/project/{id}")
    public void deleteProject(@PathVariable Integer id) throws ProjectException, TicketException{
        pService.deleteProject(id);
    }

    //allow to delete a ticket thanks to his id
    @DeleteMapping("/ticket/{idTicket}")
    public void deleteTicket(@PathVariable Integer idTicket) throws TicketException{
        tService.deleteTicket(idTicket);
    }

    //allow to get an user in the databse thanks to the principal paramaters of spring security. 
    @GetMapping("/me")
    public ResponseEntity<MyUserPrincipal> getUserProfile(){
        MyUserPrincipal myUser = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ResponseEntity.ok(myUser);
    }
}
