package zenika.evaluation.back.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import zenika.evaluation.back.dao.ProjectRepository;
import zenika.evaluation.back.dao.TicketRepository;
import zenika.evaluation.back.dto.ProjectDto;
import zenika.evaluation.back.dto.TicketDto;
import zenika.evaluation.back.exception.ProjectException;
import zenika.evaluation.back.exception.TicketException;
import zenika.evaluation.back.mapper.ProjectMapper;
import zenika.evaluation.back.mapper.TicketMapper;
import zenika.evaluation.back.model.Project;

@RestController
@RequestMapping("api/project")
//Controller for all the routes without being authenticated
public class ProjectController {

    ProjectRepository projectRepository; 
    ProjectMapper pMapper; 
    TicketMapper tMapper; 
    TicketRepository tRepository; 

    public ProjectController(ProjectRepository projectRepository, ProjectMapper pMapper, TicketMapper tMapper, TicketRepository tRepository){
        this.projectRepository = projectRepository; 
        this.pMapper = pMapper; 
        this.tMapper = tMapper; 
        this.tRepository = tRepository; 
    }
    //get all the project
    @GetMapping
    public ResponseEntity<List<ProjectDto>> getproject(){
        List<ProjectDto> projectDtos = new ArrayList<>();
        for(Project project : projectRepository.findAll()){
            projectDtos.add(pMapper.projectToProjectDto(project));
        }
        return ResponseEntity.ok(projectDtos);
    }

    //get a project by his id
    @GetMapping("{id}")
    public ResponseEntity<ProjectDto> getAProject(@PathVariable Integer id) throws ProjectException{
        return ResponseEntity.ok(pMapper.projectToProjectDto(projectRepository.findById(id).orElseThrow(()-> new ProjectException("project not found"))));
    }
    
    //get a ticket by his id
    @GetMapping("ticket/{id}")
    public ResponseEntity<TicketDto> getATicket(@PathVariable Integer id) throws TicketException{
        return ResponseEntity.ok(tMapper.ticketToTicketDto(tRepository.findById(id).orElseThrow(()-> new TicketException("Ticket not found"))));
    }
}
