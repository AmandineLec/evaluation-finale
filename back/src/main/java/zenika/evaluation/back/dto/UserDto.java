package zenika.evaluation.back.dto;
import java.util.ArrayList;
import java.util.List;

import lombok.NoArgsConstructor;


@NoArgsConstructor
public class UserDto {
    private Integer id; 
    private String username; 
    private String password; 
    private String firstName; 
    private String lastName; 
    private String mail;
    List<ProjectDto> projects = new ArrayList<>();
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getMail() {
        return mail;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }
    public List<ProjectDto> getProjects() {
        return projects;
    }
    public void setProjects(List<ProjectDto> projects) {
        this.projects = projects;
    }

    
}
