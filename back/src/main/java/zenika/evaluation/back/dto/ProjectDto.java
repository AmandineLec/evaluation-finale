package zenika.evaluation.back.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@Getter
@Setter
public class ProjectDto {
    private Integer id; 
    private String name; 
    private String description;
    private LocalDate date = LocalDate.now();
    List<TicketDto> tickets = new ArrayList<>();
    private UserDto user; 
}
