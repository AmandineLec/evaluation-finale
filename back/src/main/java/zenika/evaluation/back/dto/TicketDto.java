package zenika.evaluation.back.dto;

import lombok.Getter;
import lombok.Setter;
import zenika.evaluation.back.enumeration.Etat;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
public class TicketDto {
    private Integer id; 
    private String title; 
    private String content;
    private LocalDate date;
    private Etat etat; 
    private ProjectDto project; 
}
