INSERT INTO "user" (username, password, mail, first_name, last_name)
VALUES ('Bobby', '$2y$10$IA6QQn2oNvTa6FlL9PVRwuR3JpaGeYicY2FbSfBzRvSYOjb0C2F0y', 'bob@mail.fr', 'Bob', 'Dupond');

INSERT INTO project (name, description, date, user_id)
VALUES ('First project', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut euismod blandit fringilla. Duis malesuada vestibulum efficitur. Fusce rhoncus volutpat lacus eu sollicitudin. Donec placerat pellentesque lectus porttitor commodo.', '2023/04/26', 1),
        ('Second project', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut euismod blandit fringilla. Duis malesuada vestibulum efficitur. Fusce rhoncus volutpat lacus eu sollicitudin. Donec placerat pellentesque lectus porttitor commodo.', '2023/04/26', 1),
        ('Third project', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut euismod blandit fringilla. Duis malesuada vestibulum efficitur. Fusce rhoncus volutpat lacus eu sollicitudin. Donec placerat pellentesque lectus porttitor commodo.', '2023/04/26', 1);

INSERT INTO ticket (title, content, date, etat, project_id)
VALUES ('My first ticket', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut euismod blandit fringilla. Duis malesuada vestibulum efficitur. Fusce rhoncus volutpat lacus eu sollicitudin. Donec placerat pellentesque lectus porttitor commodo.', '2023/04/22', 0, 1),
        ('My second ticket', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut euismod blandit fringilla. Duis malesuada vestibulum efficitur. Fusce rhoncus volutpat lacus eu sollicitudin. Donec placerat pellentesque lectus porttitor commodo.', '2022/04/26', 0, 1),
        ('My third ticket', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut euismod blandit fringilla. Duis malesuada vestibulum efficitur. Fusce rhoncus volutpat lacus eu sollicitudin. Donec placerat pellentesque lectus porttitor commodo.', '2021/03/04', 1, 1 ), 
        ('My fourth ticket', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut euismod blandit fringilla. Duis malesuada vestibulum efficitur. Fusce rhoncus volutpat lacus eu sollicitudin. Donec placerat pellentesque lectus porttitor commodo.', '2022/07/26', 0, 1);
INSERT INTO user_projects (user_id, projects_id)
VALUES (1,1),
        (1,2),
        (1,3);

INSERT INTO project_tickets (project_id, tickets_id)
VALUES (1,1),
        (1,2),
        (1, 3),
        (1,4);