import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../service/authentication.service';


@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
    constructor(private readonly authenticationService: AuthenticationService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const auth = this.authenticationService.getCurrentUserBasicAuthentication()
        // add authorization header with basic auth credentials if available
        if (auth) {
            request = request.clone({
                setHeaders: {
                    Authorization: auth
                }
            });
        }

        return next.handle(request);
    }
}
