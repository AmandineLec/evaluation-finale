import { Ticket } from "./ticket";
import { User } from "./user";

export interface Project {
    id :number, 
    name :string; 
    description :string;
    date :Date,
    tickets :Ticket[],
    user :User
}

export interface ProjectDto{
    name : string; 
    description : string; 
}

