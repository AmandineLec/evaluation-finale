import { Project } from "./project";

export interface User {
    id? :number; 
    username :string; 
    password :string; 
    firstName? :string, 
    lastName? :string, 
    mail? :string;
    projects? :Project[];
}

export interface UserDto{
    username :string; 
    password :string; 
    firstName :string, 
    lastName :string, 
    mail :string;
}
