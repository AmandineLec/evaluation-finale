import { Etat } from "../enum/etat"

export interface Ticket {
    id :number,
    title :string, 
    content :string, 
    date : Date, 
    etat :Etat 
}

export interface TicketDto{
    title :string, 
    content :string,
    etat :Etat 
}
