export const getTrad = (key: string): string => {
    if (Object.hasOwn(I18n, key)) {
        return I18n[key]
    }

    return 'non traduit'
}

const I18n: Record<string, string> = {
    id : 'identifiant',
    name :'nom', 
    description :'description', 
    title :'titre', 
    content :'contenu',  
    date :'date', 
    user :'utilisateur', 
    username :'pseudo',
    firstName : 'prénom',
    lastName :'nom de famille',
    password : 'mot de passe',
    mail : 'email',
    etat : "état"

}