import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/layout/header/header.component';
import { HomeComponent } from './page/home/home.component';
import { LoginComponent } from './page/login/login.component';
import {HttpClientModule} from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectDetailsComponent } from './page/project-details/project-details.component';
import { NewProjectComponent } from './page/new-project/new-project.component';
import { ModifyProjectComponent } from './page/modify-project/modify-project.component';
import { FormProjectComponent } from './component/form-project/form-project.component';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OrderByPipe } from './pipe/order-by.pipe';
import { TicketDetailsComponent } from './page/ticket-details/ticket-details.component';
import { NewTicketComponent } from './page/new-ticket/new-ticket.component';
import { ModifyTicketComponent } from './page/modify-ticket/modify-ticket.component';
import { FormTicketComponent } from './component/form-ticket/form-ticket.component';
import { InscriptionComponent } from './page/inscription/inscription.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    ProjectDetailsComponent,
    NewProjectComponent,
    ModifyProjectComponent,
    FormProjectComponent,
    OrderByPipe,
    TicketDetailsComponent,
    NewTicketComponent,
    ModifyTicketComponent,
    FormTicketComponent,
    InscriptionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [{ provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2500 } }],
  bootstrap: [AppComponent]
})
export class AppModule { }
