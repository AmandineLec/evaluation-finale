import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Etat } from 'src/app/enum/etat';
import { getTrad } from 'src/app/model/constants';
import { TicketDto } from 'src/app/model/ticket';

@Component({
  selector: 'app-form-ticket',
  templateUrl: './form-ticket.component.html',
  styleUrls: ['./form-ticket.component.scss']
})
export class FormTicketComponent implements OnChanges{

  etats = Etat; 
  enumKeys :string[] = [];

  constructor(){
    this.enumKeys = Object.values(this.etats);
  }

  form :FormGroup = new FormGroup({
    title:new FormControl('', [Validators.required, Validators.maxLength(50)]),
    content:new FormControl('', [Validators.required, Validators.maxLength(500)]), 
    etat:new FormControl('')
  })

  @Input() ticket ! :TicketDto; 
  @Output() emitForm = new EventEmitter<TicketDto>()
  
  errors :string[] = []; 
  
  //Lors des update, on charge le formulaire avec les données du projet que l'on veut modifier
  ngOnChanges(changes: SimpleChanges): void {
    this.form.patchValue({
      title : this.ticket.title,
      content : this.ticket.content,  
      etat : this.ticket.etat
    })
  }
  
  //On envoie le formulaire qui a été remplis
  submit(){
    //on réinitialise le tableau des erreurs à zéro
    this.errors = [];
    //on vérifie que le formulaire est valide
    if(this.form.valid){
      //On stocke les valurs du formulaire dans une constante
      const values = this.form.value as TicketDto;
      //On attribue ces valeurs à l'objet project 
      this.ticket = values; 
      this.ticket.etat = (<any>Etat)[this.form.value.etat];
      //on envoie ensuite le formulaire via l'évènement
      this.emitForm.emit(this.ticket);
    }
    //On envoie un message d'erreur si un des champs requis n'est pas remplis ou ne remplis pas les conditions 
    Object.entries(this.form.controls).forEach(([key, control]) => {
      if (control.errors) {
        if (Object.hasOwn(control.errors, 'required')) {
          this.errors.push("Champ" + getTrad(key) + " : le champ est obligatoire")
        }
        if (Object.hasOwn(control.errors, 'maxlength')) {
          let  length = this.form.value["description"];
          this.errors.push("Champ " + getTrad(key) + " : la valeur ne doit pas avoir plus de" + control.errors['maxlength'].requiredLength + "caractères. Nombre de caractères actuels : " + length.length)
        }
      }
    })
    this.form.markAllAsTouched()
  }
}
