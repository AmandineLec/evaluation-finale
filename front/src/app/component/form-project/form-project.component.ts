import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { getTrad } from 'src/app/model/constants';
import { ProjectDto } from 'src/app/model/project';

@Component({
  selector: 'app-form-project',
  templateUrl: './form-project.component.html',
  styleUrls: ['./form-project.component.scss']
})
export class FormProjectComponent implements OnChanges{

  constructor(){}

form :FormGroup = new FormGroup({
  name:new FormControl('', [Validators.required]),
  description:new FormControl('', [Validators.required, Validators.maxLength(500)])
})

@Input() project ! :ProjectDto; 
@Output() emitForm = new EventEmitter<ProjectDto>()

errors :string[] = []; 

//Lors des update, on charge le formulaire avec les données du projet que l'on veut modifier
ngOnChanges(changes: SimpleChanges): void {
  this.form.patchValue({
    name : this.project.name,
    description : this.project.description
  })
}

//On envoie le formulaire qui a été remplis
submit(){
  //on réinitialise le tableau des erreurs à zéro
  this.errors = [];
  //on vérifie que le formulaire est valide
  if(this.form.valid){
    //On stocke les valurs du formulaire dans une constante
    const values = this.form.value as ProjectDto;
    //On attribue ces valeurs à l'objet project 
    this.project = values; 
    //on envoie ensuite le formulaire via l'évènement
    this.emitForm.emit(this.project);
  }
  //On envoie un message d'erreur si un des champs requis n'est pas remplis ou ne remplis pas les conditions 
  Object.entries(this.form.controls).forEach(([key, control]) => {
    if (control.errors) {
      if (Object.hasOwn(control.errors, 'required')) {
        this.errors.push("Champ" + getTrad(key) + " : le champ est obligatoire")
      }
      if (Object.hasOwn(control.errors, 'maxlength')) {
        let  length = this.form.value["description"];
        this.errors.push("Champ " + getTrad(key) + " : la valeur ne doit pas avoir plus de" + control.errors['maxlength'].requiredLength + "caractères. Nombre de caractères actuels : " + length.length)
      }
    }
  })
  this.form.markAllAsTouched()
}

}
