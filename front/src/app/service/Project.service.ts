import { Injectable } from '@angular/core'
import {HttpClient} from "@angular/common/http";
import {Observable, catchError, of, tap} from "rxjs";
import { Project, ProjectDto } from '../model/project';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Ticket, TicketDto } from '../model/ticket';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private projectsUrl = "/api/project" //Url to the api
  private projectUserUrl = "/api/user/project"
  private ticketUserUrl = "/api/user/ticket" 

  constructor(private http :HttpClient,private _snackBar: MatSnackBar) { }

  //Get all the projects
  getProjects() :Observable<Project[]>{
    return this.http.get<Project[]>(this.projectsUrl); 
  }

  //Get a project by his id
  getProjectsById(id :number) :Observable<Project>{
    return this.http.get<Project>(this.projectsUrl + "/" + id);
  }

  //Can add a project
  addProject(project :ProjectDto) :Observable<Project|undefined>{
    return this.http.post<Project>(this.projectUserUrl, project).pipe(
      tap(()=> {
        this._snackBar.open("the project has been added")
      }),
      catchError((error)=> {
        this._snackBar.open("Error, please retry")
        return of(undefined)
      })
    )
  }
  
  //Can delete a project
  deleteProject(id :number) :Observable<Project | undefined>{
    return this.http.delete<Project>(this.projectUserUrl + "/" +id).pipe(
      tap(()=> {
        return this._snackBar.open("the project has been deleted")
      }),
      catchError((error)=> {
        this._snackBar.open("Error, please retry")
        return of(undefined)
      })
    )
  }

  //can modify a project
  patchProject(project :ProjectDto, id :number) :Observable<Project | undefined>{
    return this.http.patch<Project>(this.projectUserUrl + "/" + id, project).pipe(
      tap(()=> {
        return this._snackBar.open("the project has been modified")
      }),
      catchError((error)=> {
        this._snackBar.open("Error, please retry")
        return of(undefined)
      })
    )
  }

  //Can add a ticket
  addTicket(ticket :TicketDto, id :number):Observable<Ticket |undefined>{
    return this.http.post<Ticket>(this.ticketUserUrl + "/" + id + "?etat=" + ticket.etat, ticket).pipe(
      tap(()=> {
        this._snackBar.open("the ticket has been added")
      }),
      catchError((error)=> {
        this._snackBar.open("Error, please retry")
        return of(undefined)
      })
    )
  }

  //Can get a ticket by his id
  getTicket(id :number):Observable<Ticket>{
    return this.http.get<Ticket>(this.projectsUrl + "/ticket/" + id)
  }

  //Can modify a ticket by his id
  patchTicket(ticket :TicketDto, id :number){
    return this.http.patch<Ticket>(this.ticketUserUrl + "/" + id + "?etat=" + ticket.etat, ticket).pipe(
      tap(()=> {
        return this._snackBar.open("the ticket has been modified")
      }),
      catchError((error)=> {
        this._snackBar.open("Error, please retry")
        return of(undefined)
      })
    )
  }

  //Can delete a ticket by his id
  deleteTicket(id :number):Observable<Ticket | undefined>{
    return this.http.delete<Ticket>(this.ticketUserUrl + "/" + id).pipe(
      tap(()=> {
        return this._snackBar.open("the ticket has been deleted")
      }),
      catchError((error)=> {
        this._snackBar.open("Error, please retry")
        return of(undefined)
      })
    )
  }
}


