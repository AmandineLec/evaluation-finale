import { Injectable } from '@angular/core';
import { User, UserDto } from '../model/user';
import { Observable, catchError, of, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor( private http :HttpClient, private _snackBar: MatSnackBar){}

  userUrl = "/api/user/me";
  inscription = "api/inscription";

  private readonly LOCAL_STORAGE_KEY = 'currentUser'

  //Allow to log in in the application. 
  login(user: User) {
    //We set the localstorage with the curent user
    localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(user));
    //We get the profil of the user in the back part and if the username is the same, the user can log in
    this.http.get<User>(this.userUrl).subscribe(userFromService => 
      { 
        if(userFromService.username == user.username){
          user = userFromService
        }
        else {
          //if the username is different, we log oug
          this.logout();
        } 
      }
      )
  }
  
  //Clean the local storage
  logout(): void {
    localStorage.removeItem(this.LOCAL_STORAGE_KEY);
  }

  //if a user is present in the local storage, we add it in the header to allow the basic authentification
  getCurrentUserBasicAuthentication(): string | undefined {
    const currentUserPlain = localStorage.getItem(this.LOCAL_STORAGE_KEY) 
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      return "Basic " + btoa(currentUser.username + ":" + currentUser.password);
    } else {
      return undefined;
    }
  }

  //Allow to add a new User in the database. 
  newUser(userDto :UserDto):Observable<User |undefined>{
    return this.http.post<User>(this.inscription, userDto).pipe(
      tap(()=> {
        this._snackBar.open("the user has been created")
      }),
      catchError((_error)=> {
        this._snackBar.open("Error, please retry")
        return of(undefined)
      })
    );
  }

}
