import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './page/home/home.component';
import { LoginComponent } from './page/login/login.component';
import { BasicAuthInterceptor } from './helpers/basic-auth.interceptors';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { ProjectDetailsComponent } from './page/project-details/project-details.component';
import { NewProjectComponent } from './page/new-project/new-project.component';
import { ModifyProjectComponent } from './page/modify-project/modify-project.component';
import { TicketDetailsComponent } from './page/ticket-details/ticket-details.component';
import { ModifyTicketComponent } from './page/modify-ticket/modify-ticket.component';
import { NewTicketComponent } from './page/new-ticket/new-ticket.component';
import { AuthGuard } from './guard/auth.guard';
import { InscriptionComponent } from './page/inscription/inscription.component';

const routes: Routes = [
  {path : '', component : HomeComponent},
  {path : 'login', component : LoginComponent},
  {path : 'project/:id', component : ProjectDetailsComponent},
  {path : 'newProject', canActivate:[AuthGuard],  component : NewProjectComponent},
  {path : 'modifyProject/:id', canActivate:[AuthGuard], component : ModifyProjectComponent},
  {path : 'ticket/:id', component : TicketDetailsComponent},
  {path : 'modifyTicket/:id', canActivate:[AuthGuard], component : ModifyTicketComponent}, 
  {path : 'newTicket/:id', canActivate:[AuthGuard], component : NewTicketComponent},
  {path : 'inscription', component : InscriptionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor,
      multi: true
    }
  ]
})
export class AppRoutingModule { }
