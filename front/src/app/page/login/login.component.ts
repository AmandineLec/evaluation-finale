import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{

  constructor(private router: Router,private authenticationService: AuthenticationService,){}
  form :FormGroup = new FormGroup({
    username :new FormControl('', [Validators.required]),
    password : new FormControl('', [Validators.required])
  });


  ngOnInit(): void {

  }

  onSubmit(){
    if(this.form.invalid){
      return; 
    }
    this.authenticationService.login({
      username : this.form.controls['username'].value,
      password : this.form.controls['password'].value
    });
    this.router.navigateByUrl("/")
  }
}
