import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ticket } from 'src/app/model/ticket';
import { ProjectService } from 'src/app/service/Project.service';

@Component({
  selector: 'app-ticket-details',
  templateUrl: './ticket-details.component.html',
  styleUrls: ['./ticket-details.component.scss']
})
export class TicketDetailsComponent implements OnInit{
  constructor(private route :ActivatedRoute, private pService : ProjectService, private router :Router){}
  id = this.route.snapshot.params['id'];
  ticket ! :Ticket; 
  user = localStorage;

  ngOnInit(): void {
    this.pService.getTicket(this.id).subscribe(ticketFromService => this.ticket = ticketFromService);
  }

  deleteTicket(){
    this.pService.deleteTicket(this.id).subscribe()
    this.router.navigate(['/'])
  }
}
