import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Etat } from 'src/app/enum/etat';
import { Project } from 'src/app/model/project';
import { ProjectService } from 'src/app/service/Project.service';


@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit{

  user = localStorage;
  etat = Etat; 

  constructor(private pService :ProjectService, private route :ActivatedRoute, private router :Router){}
  id = this.route.snapshot.params['id'];
  project! :Project;

  ngOnInit(): void {
    this.pService.getProjectsById(this.id).subscribe(projectFromService => this.project = projectFromService);
  }

  deleteProject(){
    this.pService.deleteProject(this.id).subscribe()
    this.router.navigate(['/'])
  }


}
