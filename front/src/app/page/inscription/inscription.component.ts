import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { getTrad } from 'src/app/model/constants';
import { User, UserDto } from 'src/app/model/user';
import { AuthenticationService } from 'src/app/service/authentication.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent {

  user! :UserDto;  
  constructor(private auth :AuthenticationService, private router :Router){}
form = new FormGroup({
  username : new FormControl('',[Validators.required]),
  password : new FormControl('', [Validators.required]),
  firstName : new FormControl('', [Validators.required]),
  lastName : new FormControl('', [Validators.required]),
  mail : new FormControl('', [Validators.required])
})
errors :string[] = [];
submit(){
  this.errors = []; 
  console.log("coucou")
  if(this.form.valid){
    const values = this.form.value;
    this.user = values as UserDto; 
    this.auth.newUser(this.user).subscribe(userFromService => {
      console.log(this.user)
      console.log(userFromService)
      if(userFromService){
        this.router.navigateByUrl("/login")
      }
    })
  }


      //On envoie un message d'erreur si un des champs requis n'est pas remplis ou ne remplis pas les conditions 
      Object.entries(this.form.controls).forEach(([key, control]) => {
        if (control.errors) {
          if (Object.hasOwn(control.errors, 'required')) {
            this.errors.push("Champ" + getTrad(key) + " : le champ est obligatoire")
          }
        }
      })
      this.form.markAllAsTouched()
}

}


