import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectDto } from 'src/app/model/project';
import { ProjectService } from 'src/app/service/Project.service';



@Component({
  selector: 'app-modify-project',
  templateUrl: './modify-project.component.html',
  styleUrls: ['./modify-project.component.scss']
})
export class ModifyProjectComponent implements OnInit{
  project ! :ProjectDto;

  constructor(private pService :ProjectService, private router :Router, private route :ActivatedRoute) {}

  id=this.route.snapshot.params['id'];

  ngOnInit(): void {
    this.pService.getProjectsById(this.id).subscribe(projectFromService => this.project = projectFromService)
  }

  onSubmit($event : any){
    this.pService.patchProject($event, this.id).subscribe(projectFromService => {
      if(projectFromService){
        this.router.navigateByUrl(`/project/${projectFromService.id}`)
      }
    })
  }
}
