import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectDto } from 'src/app/model/project';
import { ProjectService } from 'src/app/service/Project.service';


@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.scss']
})
export class NewProjectComponent {
  project ! : ProjectDto;
  constructor(private projectService :ProjectService, private router :Router){}

  onSubmit($event : any){
    console.log("coucou" + $event.etat)
    this.projectService.addProject($event).subscribe(projectFromService => {
      if(projectFromService){
        this.router.navigateByUrl(`/project/${projectFromService.id}`)
      }
    })
  }

}
