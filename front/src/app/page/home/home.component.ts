
import { AfterViewInit, Component} from '@angular/core';
import { Project } from 'src/app/model/project';
import { ProjectService } from 'src/app/service/Project.service';





@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit{

  projects :Project[] = [];
  user = localStorage;
  constructor(private pService :ProjectService){}
  ngAfterViewInit(): void {
    this.pService.getProjects().subscribe(projectFromService => this.projects = projectFromService);
  }

  ngOnInit(): void {
    this.pService.getProjects().subscribe(projectFromService => this.projects = projectFromService);
  }



  


}
