import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Etat } from 'src/app/enum/etat';
import { TicketDto } from 'src/app/model/ticket';
import { ProjectService } from 'src/app/service/Project.service';

@Component({
  selector: 'app-modify-ticket',
  templateUrl: './modify-ticket.component.html',
  styleUrls: ['./modify-ticket.component.scss']
})
export class ModifyTicketComponent implements OnInit{
  ticket ! :TicketDto;

  constructor(private pService :ProjectService, private router :Router, private route :ActivatedRoute) {}

  id=this.route.snapshot.params['id'];

  ngOnInit(): void {
    this.pService.getTicket(this.id).subscribe(ticketFromService => this.ticket = ticketFromService)
  }

  onSubmit($event : any){
    this.pService.patchTicket($event, this.id).subscribe(ticketFromService => {
      if(ticketFromService){
        this.router.navigateByUrl(`/ticket/${ticketFromService.id}`)
      }
    })
  }
}
