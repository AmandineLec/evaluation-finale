import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TicketDto } from 'src/app/model/ticket';
import { ProjectService } from 'src/app/service/Project.service';


@Component({
  selector: 'app-new-ticket',
  templateUrl: './new-ticket.component.html',
  styleUrls: ['./new-ticket.component.scss']
})
export class NewTicketComponent {
  ticket ! : TicketDto;
  constructor(private projectService :ProjectService, private router :Router, private route :ActivatedRoute){}
  id = this.route.snapshot.params['id']
  onSubmit($event : any){
    this.projectService.addTicket($event, this.id).subscribe(ticketFromService => {
      if(ticketFromService){
        this.router.navigateByUrl(`/ticket/${ticketFromService.id}`)
      }
    })
  }

}
